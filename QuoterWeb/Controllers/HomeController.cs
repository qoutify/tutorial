﻿using System.Web.Mvc;

namespace MvcCricketer.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Welcome()
        {
            ViewBag.Message = "Your Welcome Page";

            return View();
        }

        public ActionResult WelcomeNy()
        {
            ViewBag.Message = "The Ny Welcome Page";

            return View();
        }

    }
}

